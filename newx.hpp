
#include <stdio.h>
#include <type_traits>

typedef enum {CPU,CUDA,VecTypeMax} VecTypeKey;
typedef enum {INT32,INT64,IntTypeMax} IntTypeKey;
typedef enum {FLOAT32,FLOAT64,FLOAT128,FloatTypeMax} FloatTypeKey;
#define VecType   VecTypeKey
#define IntType   typename
#define FloatType typename


//  Vec is the delegator vector pointer (this is the only class the user sees)

typedef class _p_Vec* Vec;

//  Veci is the base of the delegated class

class Veci {
  public:
    int           key; // allows converting to the <VecType,IntType,FloatType> pair
    const char    *vectypename = "none";
    VecTypeKey    vectypekey;
    IntTypeKey    inttype;
    FloatTypeKey  floattype;
    Vec           parent;
};

//  Vecic are the implementation classes

template <VecType vT,IntType iT,FloatType fT> class Vecic;

//  VecBase are the methods in the implementation classes (because C++ class specialization does no inherit from the unspecialized class have to use a macro)

#define VecBase(vT,iT,fT)\
  template<IntType iiT>                                          int VecSetLength(iiT);\
  template<IntType iiT>                                          int VecSetWidth(iiT);\
  template<IntType iiT,IntType iiiT,FloatType ffT>               int VecSetValues(iiT,iiiT*,ffT*); \
  template<VecType vvT,IntType iiT,FloatType ffT,FloatType fffT> int VecDot(Vecic<vvT,iiT,ffT>* ,fffT*); \
  template<VecType vvT,IntType iiT,FloatType ffT,FloatType fffT> int VecAXPY(fffT*,Vecic<vvT,iiT,ffT>*);\
  template<VecType vvT,IntType iiT,FloatType ffT>                Vecic<vT,iT,fT>* Transport(Vecic<vvT,iiT,ffT>*);\
  iT length = -1,width = 1;

template <VecType vT,IntType iT,FloatType fT> class Vecic: public Veci {
  public:
  VecBase(vT,iT,fT)
};

// _p_Vec is the private representation of the delegator class

class _p_Vec {
 public:
  VecType current;
  Veci    *vec[VecTypeMax];
  // MPI_Comm comm;
};


template <IntType iT >                           int VecSetLengthi (Vec,iT);
template <IntType iT, IntType iiT, FloatType fT> int VecSetValuesi (Vec,iT, iiT*, fT*);
template <IntType iT>                            int VecSetWidthi (Vec,iT);
template <FloatType fT>                          int VecDoti (Vec,Vec,fT*);
template <FloatType fT>                          int VecAXPYi (Vec,fT*,Vec);
template <FloatType fT>                          int VecAXPYi (Vec,fT,Vec);

template <IntType iT>                            int VecSetLength (Vec v,iT i) {static_assert(std::is_integral<iT>::value,"Requires argument 2 be integral");return VecSetLengthi(v,i);};
template <IntType iT>                            int VecSetWidth (Vec v,iT i) {static_assert(std::is_integral<iT>::value,"Requires argument 2 be integral");return VecSetWidthi(v,i);};
template <IntType iT, IntType iiT, FloatType fT> int VecSetValues (Vec v,iT n, iiT* i, fT* a) {static_assert(std::is_integral<iT>::value,"Requires argument 2 be integral");
                                                                                               static_assert(std::is_integral<iiT>::value,"Requires argument 3 be integral");return VecSetValuesi(v,n,i,a);};
template <IntType iiT, FloatType fT>             int VecSetValues(Vec v,iiT i,fT a) {iiT tmp = 1, ival = i; fT aval = 1; return VecSetValues(v,&tmp,&ival,&aval);}
template <FloatType fT>                          int VecDot (Vec v,Vec w,fT* d) {return VecDoti(v,w,d);}
template <FloatType fT>                          int VecAXPY (Vec v,fT* d,Vec w) {return VecAXPYi(v,d,w);}
template <FloatType fT>                          int VecAXPY (Vec v,fT d,Vec w) {fT dd = d; return VecAXPY(v,&dd,w);}

// Declares public constructor for Vec

template <VecType vtype,IntType iT,FloatType fT> Vec newVec(void);
