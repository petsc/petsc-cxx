
#include <stdio.h>
#include <assert.h>

// Dispatches based on the vectors' type

#define DispatchOne(v,function,...)\
  switch (v->vec[v->current]->key) {\
  case 0:\
    {\
      Vecic<CPU,int,float>* iv = (Vecic<CPU,int,float>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 1:\
    {\
      Vecic<CPU,int,double>* iv = (Vecic<CPU,int,double>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 2:                                                                \
    {\
      Vecic<CPU,int64_t,float>* iv = (Vecic<CPU,int64_t,float>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 3:\
    {\
      Vecic<CPU,int64_t,double>* iv = (Vecic<CPU,int64_t,double>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 4:\
    {\
      Vecic<CUDA,int,float>* iv = (Vecic<CUDA,int,float>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 5:\
    {\
      Vecic<CUDA,int,double>* iv = (Vecic<CUDA,int,double>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 6:\
    {\
      Vecic<CUDA,int64_t,float>* iv = (Vecic<CUDA,int64_t,float>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  case 7:\
    {\
      Vecic<CUDA,int64_t,double>* iv = (Vecic<CUDA,int64_t,double>*) v->vec[v->current];\
      return iv->template function(__VA_ARGS__);\
    }\
    break;\
  default:\
    return 0;\
    break;\
  }

#define DispatchTwo(v,w,function,...)\
  switch (v->vec[v->current]->key) {\
  case 0:\
    {\
      Vecic<CPU,int,float>* iw = (Vecic<CPU,int,float>*) w->vec[w->current];\
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 1:\
    {\
      Vecic<CPU,int,double>* iw = (Vecic<CPU,int,double>*) w->vec[w->current];\
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 2:\
    {\
      Vecic<CPU,int64_t,float>* iw = (Vecic<CPU,int64_t,float>*) w->vec[w->current]; \
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 3:\
    {\
      Vecic<CPU,int64_t,double>* iw = (Vecic<CPU,int64_t,double>*) w->vec[w->current];\
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 4:\
    {\
      Vecic<CUDA,int,float>* iw = (Vecic<CUDA,int,float>*) w->vec[w->current];\
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 5:\
    {\
      Vecic<CUDA,int,double>* iw = (Vecic<CUDA,int,double>*) w->vec[w->current];\
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 6:\
    {\
      Vecic<CUDA,int64_t,float>* iw = (Vecic<CUDA,int64_t,float>*) w->vec[w->current]; \
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 7:\
    {\
      Vecic<CUDA,int64_t,double>* iw = (Vecic<CUDA,int64_t,double>*) w->vec[w->current];\
      DispatchOne(v,function,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  default:\
    return 0;\
    break;\
  }

#define DispatchTwoSkip(v,w,function,d,...)     \
  switch (v->vec[v->current]->key) {\
  case 0:\
    {\
      Vecic<CPU,int,float>* iw = (Vecic<CPU,int,float>*) w->vec[w->current];\
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 1:\
    {\
      Vecic<CPU,int,double>* iw = (Vecic<CPU,int,double>*) w->vec[w->current];\
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 2:\
    {\
      Vecic<CPU,int64_t,float>* iw = (Vecic<CPU,int64_t,float>*) w->vec[w->current]; \
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 3:\
    {\
      Vecic<CPU,int64_t,double>* iw = (Vecic<CPU,int64_t,double>*) w->vec[w->current];\
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 4:\
    {\
      Vecic<CUDA,int,float>* iw = (Vecic<CUDA,int,float>*) w->vec[w->current];\
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 5:\
    {\
      Vecic<CUDA,int,double>* iw = (Vecic<CUDA,int,double>*) w->vec[w->current];\
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 6:\
    {\
      Vecic<CUDA,int64_t,float>* iw = (Vecic<CUDA,int64_t,float>*) w->vec[w->current]; \
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  case 7:\
    {\
      Vecic<CUDA,int64_t,double>* iw = (Vecic<CUDA,int64_t,double>*) w->vec[w->current];\
      DispatchOne(v,function,d,iw __VA_OPT__(,) __VA_ARGS__)\
    }\
    break;\
  default:\
    return 0;\
    break;\
  }


// Defines the public functions that operate on the Vec

template <IntType iT, IntType iiT, FloatType fT> int VecSetValuesi (Vec v,iT a, iiT *id, fT *b)
{
  DispatchOne(v,VecSetValues,a,id,b);
}

template <IntType iT> int VecSetLengthi (Vec v,iT a)
{
  DispatchOne(v,VecSetLength,a);
}

template <IntType iT> int VecSetWidthi (Vec v,iT a)
{
  DispatchOne(v,VecSetWidth,a);
}

template <FloatType fT> int VecDoti (Vec v,Vec w,fT *d)
{
  DispatchTwo(v,w,VecDot,d);
}

template <FloatType fT> int VecAXPYi (Vec v,fT *d,Vec w)
{
  DispatchTwoSkip(v,w,VecAXPY,d);
}



