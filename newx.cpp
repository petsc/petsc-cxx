#include "newx.hpp"

// Utility to convert a Vecic<> to its key

template <VecType vT,IntType iT,FloatType fT> int ComputeKey(void) {return FloatTypeMax*(vT*IntTypeMax + sizeof(iT)/4 - 1) + sizeof(fT)/4 - 1;}
template <VecType vT,IntType iT,FloatType fT> int ComputeKey(Vecic<vT,iT,fT> *v) {return ComputeKey<vT,iT,fT>();}

template <VecType vT,IntType iT,FloatType fT> int addVec(Vec);
template <VecType vT,IntType iT,FloatType fT> int switchVec(Vec);

// Specialization of vectors for CPU

template <IntType iT,FloatType fT> class Vecic<CPU,iT,fT>: public Veci {
  public:
    VecBase(CPU,iT,fT)
    template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> typename std::enable_if_t<CPU == vT,int> joe(Vecic<vT,iiiT,ffT> *w,fffT *b)
{
  return 0;
}
  template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> typename std::enable_if_t<CPU != vT,int> joe(Vecic<vT,iiiT,ffT> *w,fffT *b)
{
  return 0;
}
private:
    fT  *array = nullptr;
};

//  Implementations for all the CPU methods


template<IntType iT,FloatType fT> template<IntType iiT> int Vecic<CPU,iT,fT>::VecSetLength(iiT len)
{
  this->length = len;
  // specifics for CPU also handle change in size
  return 0;
}

template<IntType iT,FloatType fT> template<IntType iiT> int Vecic<CPU,iT,fT>::VecSetWidth(iiT wid)
{
  this->width = wid;
  // specifics for CPU
  return 0;
}

template<IntType iT,FloatType fT> template<IntType iiT,IntType iiiT,FloatType ffT> int Vecic<CPU,iT,fT>::VecSetValues(iiT a, iiiT *idx,ffT *b)
{
  return 0;
}


template<IntType iT,FloatType fT> template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> int Vecic<CPU,iT,fT>::VecAXPY(fffT *b,Vecic<vT,iiiT,ffT> *w)
{
  return 0;
}

template<IntType iT,FloatType fT> template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> int Vecic<CPU,iT,fT>::VecDot(Vecic<vT,iiiT,ffT> *w,fffT *b)
{
  return 0;
  }

/*
  template<int N,class T=int>
    typename std::enable_if<N==1, T>::type
    f(T t) {return 1;}

  template<int N,class T=int>
    typename std::enable_if<N!=1, T>::type
    f(T t) {return 2;}

template<IntType iT,FloatType fT> template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> typename std::enable_if<std::is_same<iT,fT>::value,int>::type Vecic<CPU,iT,fT>::VecDot(Vecic<vT,iiiT,ffT> *w,fffT *b)
{
  return 0;
}



template<IntType iT,FloatType fT> template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT,typename = typename std::enable_if<(vT != CPU)>::type > int Vecic<CPU,iT,fT>::VecDot(Vecic<vT,iiiT,ffT> *w,fffT *b)
{
  if (vT != CPU) {
    switchVec<CPU,iT,fT>(w->parent);
    ::VecDot(this->parent,w->parent,b);
    return 0;
  }
  return 0;
  }
*/

// Specialization of vectors for CUDA

template <IntType iT,FloatType fT> class Vecic<CUDA,iT,fT>: public Veci {
  public:
    VecBase(CUDA,iT,fT)
  private:
};

//  Implementations for all the CUDA methods


template<IntType iT,FloatType fT> template<IntType iiT> int Vecic<CUDA,iT,fT>::VecSetLength(iiT len)
{
  this->length = len;
  // specifics for CPU also handle change in size
  return 0;
}

template<IntType iT,FloatType fT> template<IntType iiT> int Vecic<CUDA,iT,fT>::VecSetWidth(iiT wid)
{
  this->width = wid;
  // specifics for CUDA
  return 0;
}

template<IntType iT,FloatType fT> template<IntType iiT,IntType iiiT,FloatType ffT> int Vecic<CUDA,iT,fT>::VecSetValues(iiT a, iiiT *id,ffT *b)
{
  switchVec<CPU,iT,fT>(this->parent);
  ::VecSetValues(this->parent,a,id,b);
  return 0;
}

template<IntType iT,FloatType fT> template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> int Vecic<CUDA,iT,fT>::VecAXPY(fffT *b,Vecic<vT,iiiT,ffT> *w)
{
  return 0;
}

template<IntType iT,FloatType fT> template<VecType vT,IntType iiiT,FloatType ffT,FloatType fffT> int Vecic<CUDA,iT,fT>::VecDot(Vecic<vT,iiiT,ffT> *w,fffT *b)
{
  return 0;
}

template<VecType vT,IntType iT,FloatType fT> template<VecType vvT,IntType iiT,FloatType ffT>  Vecic<vT,iT,fT>* Vecic<vT,iT,fT>::Transport(Vecic<vvT,iiT,ffT> *w) {return 0;}

// Construct a Vec and add an implementation (delegated) vector

template <VecType vT,IntType iT,FloatType fT> Vec newVec(void) {
  Vec vec = new _p_Vec;
  vec->vec[vT] = (Veci*) new Vecic<vT,iT,fT> ;
  vec->vec[vT]->parent = vec;
  vec->current = vT;
  return vec;
}

// add to a Vec an new type that is delegated

template <VecType vT,IntType iT,FloatType fT> int addVec(Vec vec) {
  if (vT == vec->current) return 0;
  if (!vec->vec[vT]) {
    vec->vec[vT] = (Veci*) new Vecic<vT,iT,fT> ;
    vec->vec[vT]->parent = vec;
  }
  return 0;
}

// change a Vec to point to a different delegated type

template <VecType vT,IntType iT,FloatType fT> int switchVec(Vec vec) {
  if (vT == vec->current) return 0;
  addVec<vT,iT,fT>(vec);
  vec->current = vT;
  return 0;
}

// Instantiations for CPU

template  Vec newVec<CPU,int,float>();
template  Vec newVec<CPU,int,double>();
template  Vec newVec<CPU,int64_t,float>();
template  Vec newVec<CPU,int64_t,double>(); 

// Instantiations for CUDA

template  Vec newVec<CUDA,int,float>();
template  Vec newVec<CUDA,int,double>();
template  Vec newVec<CUDA,int64_t,float>();
template  Vec newVec<CUDA,int64_t,double>(); 

#include "dispatch.hpp"

template int VecAXPYi(Vec,float*,Vec);
template int VecAXPYi(Vec,double*,Vec);
template int VecDoti(Vec,Vec,float*);
template int VecDoti(Vec,Vec,double*);
template int VecSetLengthi(Vec,int);
template int VecSetLengthi(Vec,int64_t);
template int VecSetWidthi(Vec,int);
template int VecSetWidthi(Vec,int64_t);
template int VecSetValuesi(Vec,int,int*,float*);
template int VecSetValuesi(Vec,int,int*,double*);
template int VecSetValuesi(Vec,int,int64_t*,float*);
template int VecSetValuesi(Vec,int,int64_t*,double*);
template int VecSetValuesi(Vec,int64_t,int*,float*);
template int VecSetValuesi(Vec,int64_t,int*,double*);
template int VecSetValuesi(Vec,int64_t,int64_t*,float*);
template int VecSetValuesi(Vec,int64_t,int64_t*,double*);

template int  Vecic<CPU,int,float>::joe<CPU,int,float,float>(Vecic<CPU,int,float>*,float*);
template int  Vecic<CPU,int,float>::joe<CUDA,int,float,float>(Vecic<CUDA,int,float>*,float*);
