
#include <stdio.h>
#include "newx.hpp"


// -----------------------------------------------------------------

int main()
{
    
  Vec v = newVec<CPU,int,double>();

  int t = 23;
  int l= VecSetLength(v,t);
  
  int64_t ii = 2;

  int64_t indices[ii] = {0,1};
  double values[ii] = {3.0,4.0};
  
  int i = VecSetValues(v,ii,indices,values);

  v = newVec<CPU,int64_t,double>();

  i = VecSetValues(v,1,indices,values);

  float f;
  i = VecDot(v,v,&f);  

  Vec z = newVec<CUDA,int,float>();

  i = VecDot(v,z,&f);  

  i = VecSetValues(z,2,indices,values);

  Vec w = newVec<CPU,int,double>();

  i = VecAXPY(v,3.0,w);

  return 0;

}
